# Dressel Jutta, (1937). Hat Jesus wirklich gelebt?, Verlag Pfeiffer & Co.



| Book   |   Metadata    |
|----------|:-------------:|
| Title: | Hat Jesus wirklich gelebt? |
| English: | Was Jesus a real man? |
| Author: | Dr. Jutta Dressel |
| Type: | Essay |
| Subtitle: |  |
| Original title: | Hat Jesus wirklich gelebt? |
| Date:  | 1937 |
| Editor: | Verlag Pfeiffer & Co. |
| Source of Scans: | [![https://archive.org/details/HatJesusWirklichGelebt/mode/2up](https://ia802602.us.archive.org/BookReader/BookReaderImages.php?zip=/0/items/HatJesusWirklichGelebt/DresselDr.Jutta-HatJesusWirklichGelebtVerlagPfeifferCo._jp2.zip&file=DresselDr.Jutta-HatJesusWirklichGelebtVerlagPfeifferCo._jp2/DresselDr.Jutta-HatJesusWirklichGelebtVerlagPfeifferCo._0000.jp2&id=HatJesusWirklichGelebt&scale=16&rotate=0)](https://archive.org/details/HatJesusWirklichGelebt/mode/2up) |
| Language: | Deutsch |

## TODO

Convert txt pages to md chapters
Ebook
Translation


## Einleitung

Seite 5-10

## I. Teil

Seite 11-13

## II. Teil

Seite 14-24

## III. Teil

Seite 25-32

## IV. Teil

Seite 33-41

## Schluẞabſchnitt

Seite 42-44
