Wegen, damit das WOrt deſſen, der mich geſandt hat, auf der ganzen
Erde bekannt werde: Ihr werdet Euren Lohn erhalten wie die Arbeiter
den ihrigen erhalten haben. Nach den guten Handlungen
ſelbſt, nicht nach ihrer Menge werdt ihr gerichtet
werden".

Mag man auch der Gleichbelohnung des Vielvermögenden und des
Wenigvermögenden nicht zuſtimmen, ſo wird ſich doch niemand dem Ein-
druck der reinen und hohen Geſinnung entziehen, die in dieſem Gleichnis
ſich zu erkennen gibt. Und man muẞ ja auch bedenken, daẞ dies eben
ein Gleichnis iſt, das nur ſagen will, er möchte die ſeeliſche Haltung
des Einzelnen, wo ſie gleich war, auch gleich behandelt ſehen. Während
nun ſo unſer Gefühl gerne dem ſinnvollen und ethiſchen Gedankenſpiel
der Erzählung folgt, finden wir in der jüdiſchen Wiedergabe eine ſinnlos
zuſammengeſtoẞene Geſchichte, aus der die Ethik gewichen iſt, und wo die
Willkür ſtatt deſſen anmaẞend auftritt. Dies iſt der jüdiſche Bibeltert
des alten indiſchen Gleichniſſes:

"Matthäus 20: Das Himmelreich iſt gleich einem Hausvater, der
am Morgen ausging, Arbeiter zu mieten in ſeinem Weinberg. Und
da er mit den Arbeitern eins ward um einen Groſchen Taglohn, ſandte
er ſie in ſeinen Weinberg, ich will euch geben was recht iſt. Und ging
aus um die dritte Stunde und ſah andere an dem Markt muẞig ſtehen.
Und er ſprach zu ihnen: Gehet ihr auch hin in den Weinberg, ich will
euch geben was recht iſt. Und ſie gingen hin. Abermals ging er aus um
die ſechſte Stunde und um die neunte Stunde und tat alſo. Um die
elfte Stunde aber ging er aus und fand andere müẞig ſtehen und
ſprach zu ihnen: Was ſteht ihr hier den ganzen Tag müẞig? Sie ſpra-
chen zu ihm: Es hat uns niemand gedingt. Er ſprach zu ihnen: Gehet
ihr auch hin in der Weinberg und was euch recht ſein wird, ſoll euch
gegeben werden. Da es nun Abend ward, ſprach der Herr zu ſeinem
Schaffner: Rufe die Arbeiter und gib ihnen den Lohn und heb an an
den Letzen bis zu den Erſten. Da kamen die um die elfte Stunde ge-
dingt waren und empfingen ein jeglicher ſeinen Groſchen. Da aber die
Erſten kamen, meinten ſie, ſie würden mehr empfangen, und ſie empfingen
auch ein jegliche ſeinen Groſchen. Und da ſie den empfingen, murrten
