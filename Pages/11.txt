# 1. Teil

Die Evangelien erzählen davon, daẞ das Leben des Jeſus viel Auf-
ſehen im jüdiſchen Volk erregte. Von der Verfolgung des Herodes zur
Zeit, da Jeſus ein kleines Kind geweſen ſein ſoll, über die vielen Wunder-
taten hin, die das Volk erregten, bis zum letzten Todesgeſang, da der
römiſche Landpfleger Pontius Pilatus auf das Treiben des Jeſus auf-
merkſam wurde und ihn zum Tode verurteilen und kreuzigen lieẞ, hat
die Geſtalt des Jeſus im Blickpunkt der Deffentlichkeit geſtanden. "Die
Kunde ging aus von ihm in alle Lande" melden die Evangelien.

So erzählt die Bibel. Wer aber, der dies als Kind lernt, lernt dazu,
daẞ die Geſchichte nichts von alledem zu melden weiẞ? daẞ, wenn man
in der zeitgenöſſiſchen Geſchichte nachforſcht, eiſiges Schweigen herrſcht
über den auftretenden jüdiſchen Gottmenſchen? Und das iſt ſo nicht
deshalb, weil es an Berichten aus jener Zeit mangelte, die dieſe Vorfälle
in Paläſtina hätten melden müſſen. Hören wir Paſtor Steudel dazu
nach ſeiner genannten Schrift: "Viele glauben, wir beſaẞen gar keine
jüdiſchen Schriften, von denen ein Bericht darüber zu erwarten wäre.
Dabei haben ein gemiſſer Joſephus und neben ihm ein Juſtus Liberias
ganze Bücher über jene ereignisreiche Zeit geſchrieben, und der Eine,
beſſen Schriften uns glücklicherweiſe erhalten ſind, regiſtriert mit pein-
licher Genauigkeit alle Greueltaten, die der ſo verhaẞte Pontius Pilatus
während ſeiner Regierungszeit geleiſtet hat"! "Wenn Joſephus der
Einzige wäre, von dem billigerweiſe eine Erwähnung des geſchichtlichen
Jeſus und der ſich anſchlieẞenden Bewegung zu erwarten wäre, ſo lieẞe
ſich nicht allzuviel dagegen einwenden. Nun iſt er aber lange nicht der
Einzige. Freilich weiẞ der Theologe v. S. daruber himwegzugleiten
und ſagt: Die Werke von Juſtus Liberias ſind übrigens verloren. Das
iſt eine Ausflucht, deren ſich auch der Theologieprofeſſor B. bedient
hat, mit der aber garnichts anzufangen iſt, denn es iſt uns das abſolut
zuverläſſige Zeugnis des gelehrten Patriarchen von Konstantinopel Pho-