Erkenntniſſe legen, die uns bei den Alten in Staunen ſetzen. Der Kün-
ſtler wird ſich aber auch nicht die Bewunderung der hochbegabten Phan-
tasie nehmen laſſen wollen, die eine Unfülle von Sagen ſchuf, uns allen
nun in ſo vieler Geſtalt ſchon längſt bekannt, wenn wir auch den
letzten Sternenſin, der ihnen zu Grunde lag, in den allermeiſten Fällen
nicht kennen lernten. Hatten doch die "Alten", die ſie uns überlieferten,
ſelber oft nur eine abgeblaẞte Ahnung von dieſen urſprünglichen Zu-
ſammenhängen, die bei den noch Aelteren (Jahrtauſende älter auch für
ſie) entſtanden waren. Edda-Sage, Griechen-Sagen, Babylonier-Sagen,
ſie alle aber gehen uns hier als Stern-Sagen nicht viel an. Im Buch
"Sternenhimmel" von A. Drews iſt darüber zu leſen. Wohl aber gehen
uns hier die engeren Sagen etwas an, die bei dem Zuſtandekommen der
Jeſus-Sage eine beſondere Rolle ſpielten.

Eins ſei aber noch vorausgeſchickt. Geht man auf dieſe älteſten Wur-
zeln zurück, ſo findet man, daẞ oft Gleiches oder Verbindendes in
uns bekannten morgen- und abendländiſchen Mythen zu finden iſt, das
nicht der Zufall ſogleich oder ähnlich hat werden laſſen können.

Hohe Sternenkentniſſe hat man auf deutſchem Boden z. B. für das
Jahr 1850 vor unſerer Zeitrechnung anzunehmen. Und manche Forſcher
glauben, daẞ nordiſche wandernde Stämme die Anfänge der Geſtirnkunde
nach Aegypten und Meſopotamien brachten und die Blüte der ägypti-
ſchen Sternenkunde zuletzt auf einen nordiſchen Einfluẞ hinweiſt. Das
iſt keine Anmaẞung, "um alles Gute in den Norden zu verlegen", ſon-
dern es ſcheint ſo mit aller ernſthaften und gewiſſenhaften Forſchung ver-
einbar oder zum mindeſten erwägbar zu ſein. Autoren will ich hierzu
jetzt nicht anführen. Da nun aber ein uralter Zuſammenhang der nor-
diſchen und der Mittelmeerkultur anzunehmen iſt, darf man ſich auch
nicht erſtaunen, wenn man bei einem Zurückgehen von einer Sage auf
ihre Urſprungform bisweilen ebenſo gut, anſtatt etwa auf eine altperſiſche
Wurzel zurückzugeben, auch auf eine Analogie bei einem alt-ägyptiſchen
Text hinweiſen kann. Wahrſcheinlich übten Sagenkreiſe nicht nur nach-
träglich, wenn Völker in engere Beziehung miteinander kamen, einen
Einfluẞ aufeinander aus, ſondern es gibt auch ein gewiſſes Grundgerüſt
das in den meiſten Sagenkreiſen verſteckt enthalten iſt, da in älteſter
